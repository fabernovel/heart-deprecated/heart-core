/**
 * List of events names
 * @see {@link https://www.typescriptlang.org/docs/handbook/enums.html}
 * @deprecated since 1.2. Use AnalysisEvents instead.
 */
const enum NotificationEvents {
  ANALYSIS_DONE = 'analysis.done',
}

export default NotificationEvents;
