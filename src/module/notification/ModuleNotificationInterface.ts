import { NotificationConstructor } from '../../model/NotificationInterface';

/**
 * Define a Notification module.
 */
export default interface ModuleNotificationInterface {
  Notification: NotificationConstructor;
}
