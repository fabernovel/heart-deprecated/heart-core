import { StorageConstructor } from '../../model/StorageInterface';

/**
 * Define a Storage module.
 */
export default interface ModuleStorageInterface {
  Storage: StorageConstructor;
}
