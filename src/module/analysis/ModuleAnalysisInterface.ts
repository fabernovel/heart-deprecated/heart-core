import { AnalysisConstructor } from '../../model/AnalysisInterface';

/**
 * Define an Analysis module.
 */
export default interface ModuleAnalysisInterface {
  Analysis: AnalysisConstructor;
}
